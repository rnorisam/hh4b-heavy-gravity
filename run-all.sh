#!/usr/bin/env bash

set -eu

OUTDIR=outputs

mkdir -p ${OUTDIR}
cd $OUTDIR
#testing taking JO files from generator directory.
$jobDir = /cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/share/DSID451xxx/
#for JO in ../MC15JobOptions/MC15*.451*.py
for JO in $jobDir/*MC15*451*MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbbb_c10_M*.py
do
    echo "running $JO"
    JO_FULL=$(readlink -e $JO)
    NAME=${JO##*/}
    RUN=$(echo $NAME | cut -d . -f 2)
    SHORTNAME=${NAME%.py}
    if [[ -d $SHORTNAME ]]; then
        echo "$SHORTNAME exists, skipping"
        continue
    fi
    mkdir -p $SHORTNAME
    (
        cd $SHORTNAME
        ln -s ../../MC15JobOptions
        Generate_tf.py --maxEvents="50" --ecmEnergy="13000" \
                       --evgenJobOpts="MC15JobOpts-00-00-42_v3.tar.gz" \
                       --firstEvent="1" \
                       --jobConfig=$JO_FULL \
                       --outputEVNTFile="output.pool.root.1" \
                       --randomSeed="4" \
                       --runNumber=$RUN \
                       --AMITag="e3820" | tee log.txt
    )
done
