#!/usr/bin/env bash

Generate_tf.py --maxEvents="500" --ecmEnergy="13000" \
               --evgenJobOpts="MC15JobOpts-00-00-42_v3.tar.gz" \
               --firstEvent="1" \
               --jobConfig="MC15JobOptions/MC15.301491.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbbb_c10_M600.py" \
               --outputEVNTFile="test.pool.root.1" \
               --randomSeed="4" \
               --runNumber="301491" \
               --AMITag="e3820"

