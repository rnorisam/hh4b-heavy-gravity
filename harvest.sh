#!/usr/bin/env bash

set -eu

TAR=ATLMCPROD-8980
XSEC_FILE=cross-sections.txt

if [[ -d $TAR ]]; then
    echo "removing $TAR"
    rm -r $TAR
fi

mkdir $TAR
touch ${TAR}/${XSEC_FILE}
cp MC15JobOptions/MC15*.451*.py $TAR

for f in outputs/**/log.txt
do
    LOGNAME=$(cut -d / -f 2 <<< $f)
    cp $f ${TAR}/${LOGNAME}.log
    XSEC=$(egrep -o 'cross-section \(nb\)= [0-9.e+-]+' $f | cut -f 3 -d ' ')
    DSID=$(sed -rn 's/.*\.(451[0-9]{3})\..*/\1/p' <<< $f)
    echo $DSID $XSEC | tee -a ${TAR}/${XSEC_FILE}
done

cp MC15JobOptions/MadGraphControl_RS_G_hh_bbbb.py $TAR

tar czf ${TAR}.tgz $TAR
